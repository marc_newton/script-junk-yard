$.fn.tabloid = function()
{
	var group = this;
	var tabs = $('<div class="tabs">');

	$(group).addClass('tabloid').children('div[data-label]').addClass('pane');

	$.each($(group).children('div[data-label]'), function(){
		tabs.append('<a>' + $(this).data('label') + '</a>');
	});

	tabs.prependTo(group).on('click', 'a', function() {
		tabs.children('a').removeClass('active');
		$(this).addClass('active');
		$(group).children('.pane').removeClass('active').eq($(this).index()).addClass('active');
	});

	tabs.children('a').first().trigger('click');
};